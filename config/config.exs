# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :graphql,
  ecto_repos: [Graphql.Repo]

# Configures the endpoint
config :graphql, Graphql.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "qQSFI8I6YzkEbV/hL4h8nTgOxhK7dl2LP0Al3C/0VN7btUBB7rK7DGZglY1Zmuxn",
  render_errors: [view: Graphql.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Graphql.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
