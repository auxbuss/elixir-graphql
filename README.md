# Graphql

Idea from [Phoenix GraphQL Tutorial with Absinthe](https://ryanswapp.com/2016/11/29/phoenix-graphql-tutorial-with-absinthe/)

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phoenix.server`
  * Visit [`localhost:4000`](http://localhost:4000) from your browser.

  * Seed the dev db with `mix run priv/repo/seeds.exs`

The API is [`here.`](http://localhost:4000/api), but it will return _No query document supplied_ without a query.

Instead, use the [`query runner`](http://localhost:4000/graphiql).

## Example queries:

    { posts { title, body } }

    { posts {
        title,
        body,
        author {
          name
        }
      }
    }

    mutation CreatePost {
      post(title: "Second", body: "We're off to a great start!", userId: 123) {
        id
      }
    }

    mutation UpdatePost {
      updatePost(
        id: 123,
        post: { title: "Third", body: "We're off to a great start!", userId: 1 }
      ) { id }
    }

    mutation DeletePost {
      deletePost(id: 14) { id }
    }

## Documentation

    $ mix docs
    $ open doc/index.html
