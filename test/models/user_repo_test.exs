defmodule Graphql.UserRepoTest do
  use Graphql.ModelCase, async: false
  alias Graphql.User

  @valid_attrs %{name: "A User", email: "fred@example.com"}

  test "converts unique_constraint on username to error" do
    insert_user(%{name: "fred"})
    attrs = Map.put(@valid_attrs, :name, "fred")
    changeset = User.changeset(%User{}, attrs)
    assert {:error, changeset} = Repo.insert(changeset)
    assert {:name, {"has already been taken", []}} in changeset.errors
  end
end
