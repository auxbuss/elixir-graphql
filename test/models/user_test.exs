defmodule Graphql.UserTest do
  use Graphql.ModelCase

  alias Graphql.User

  @valid_attrs %{email: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "changeset does not accept long names" do
    attrs = Map.put(@valid_attrs, :name, String.duplicate("a", 30))
    assert {:name, "should be at most 20 character(s)"} in errors_on(%User{}, attrs)
  end
end
