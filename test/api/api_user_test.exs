defmodule Graphql.ApiUserTest do
  use Graphql.ModelCase
  alias Graphql.{Schema, Post}

  test "create a post" do
    {:ok, author} = insert_user(%{name: "Josh Adams"})

    query = """
      mutation CreatePost {
        post(title: "Second", body: "We're off to a great start!", userId: #{author.id}) {
          id
        }
      }
    """

    result = Absinthe.run(query, Schema)
    post   = Post |> first |> Repo.one

    assert result == {:ok, %{data: %{"post" => %{"id" => "#{post.id}"}}}}
  end

  test "update a post" do
    {:ok, author} = insert_user(%{name: "Josh Adams"})

    title = "Third"
    body  = "We're off to a great start!"
    {:ok, post} = insert_post(author, title: "Testing", body: "#{body}")

    query = """
      mutation UpdatePost {
        updatePost(
          id: #{post.id},
          post: { title: "#{title}", body: "#{body}", userId: #{author.id}}
        ) { id }
      }
    """

    result = Absinthe.run(query, Schema)
    post   = Post |> first |> Repo.one

    assert result == {:ok, %{data: %{"updatePost" => %{"id" => "#{post.id}"}}}}
    assert post.title == title
    assert post.body == body
  end

  test "delete a post" do
    {:ok, author} = insert_user(%{name: "Josh Adams"})
    {:ok, post} = insert_post(author, title: "Testing")

    query = """
      mutation DeletePost {
        deletePost(
          id: #{post.id}
        ) { id }
      }
    """

    result = Absinthe.run(query, Schema)
    assert result == {:ok, %{data: %{"deletePost" => %{"id" => "#{post.id}"}}}}
    assert Repo.aggregate(Post, :count, :id) == 0
  end

  test "fail to delete a post" do
    {:ok, author} = insert_user(%{name: "Josh Adams"})
    {:ok, _} = insert_post(author, title: "Testing")

    query = """
      mutation DeletePost {
        deletePost(
          id: 9999
        ) { id }
      }
    """

    result = Absinthe.run(query, Schema)
    assert result == {:ok, %{data: %{"deletePost" => nil},
      errors: [%{id: 9999, locations: [%{column: 0, line: 2}],
      message: "Post delete failed", path: ["deletePost"]}]}}
    assert Repo.aggregate(Post, :count, :id) == 1
  end

  test "fail post update: missing author" do
    {:ok, author} = insert_user(%{name: "Josh Adams"})

    title = "Third"
    body  = "We're off to a great start!"
    {:ok, post} = insert_post(author, title: "Testing", body: "#{body}")

    query = """
      mutation UpdatePost {
        updatePost(
          id: #{post.id},
          post: { title: "#{title}", body: "#{body}", userId: 9999 }
        ) { id }
      }
    """

    result = Absinthe.run(query, Schema)

    assert result == {:ok, %{
      data: %{"updatePost" => nil},
      errors: [%{locations: [%{column: 0, line: 2}],
      message: "Post update failed", path: ["updatePost"], id: post.id}]}
    }
  end

  test "retrieve the list of users" do
    {:ok, josh} = insert_user(%{name: "Josh Adams"})
    {:ok, adam} = insert_user(%{name: "Adam Dill"})

    query = "{ users{ id, name } }"

    expected_users = [
        %{"id" => "#{josh.id}", "name" => josh.name},
        %{"id" => "#{adam.id}", "name" => adam.name}
      ]

    assert Absinthe.run(query, Schema) == {:ok, %{data: %{"users" => expected_users}}}
  end

  test "retrieve a single user" do
    {:ok, josh} = insert_user(%{name: "Josh Adams"})

    query       = "{ user(id: \"#{josh.id}\") { id, name } }"
    expectation = %{"id" => "#{josh.id}", "name" => josh.name}

    assert Absinthe.run(query, Schema) == {:ok, %{data: %{"user" => expectation}}}
  end

  test "retrieve a single post" do
    {:ok, user} = insert_user(%{name: "Josh Adams"})
    {:ok, post} = insert_post(user, title: "Testing")

    query       = "{ post(id: \"#{post.id}\") { id, title, author{id} } }"
    expectation = %{"id" => "#{post.id}", "title" => post.title, "author" => %{"id" => "#{user.id}"}}

    assert Absinthe.run(query, Schema) == {:ok, %{data: %{"post" => expectation}}}
  end

  test "send empty query" do
    query = ""
    assert Absinthe.run(query, Schema) == {:ok, %{errors: [%{message: "No operations provided."}]}}
  end
end
