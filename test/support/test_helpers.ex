defmodule Graphql.TestHelpers do
  alias Graphql.{Repo, User}

  def insert_user(attrs \\ %{}) do
    changes = Map.merge(%{
      name:  "Some User",
      email: "fred@example.com",
    }, attrs)

    %User{}
    |> User.changeset(changes)
    |> Repo.insert()
  end

  def insert_post(post, attrs \\ %{}) do
    post
    |> Ecto.build_assoc(:posts, attrs)
    |> Repo.insert()
  end
end
