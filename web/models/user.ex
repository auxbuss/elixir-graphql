defmodule Graphql.User do
  use Graphql.Web, :model

  schema "users" do
    field :name, :string
    field :email, :string
    has_many :posts, Graphql.Post

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :email])
    |> validate_required([:name, :email])
    |> validate_length(:name, min: 1, max: 20)
    |> unique_constraint(:name)
  end
end
