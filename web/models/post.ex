defmodule Graphql.Post do
  use Graphql.Web, :model

  schema "posts" do
    field :title, :string
    field :body, :string
    belongs_to :user, Graphql.User

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :body, :user_id])
    |> validate_required([:title, :body, :user_id])
    |> foreign_key_constraint(:user_id)
  end
end
