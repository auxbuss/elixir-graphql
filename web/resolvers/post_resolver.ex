defmodule Graphql.PostResolver do
  alias Graphql.{Repo, Post}

  def all(_args, _info) do
    {:ok, Repo.all(Post)}
  end

  def first(%{id: id}, _info) do
    case Repo.get(Post, id) do
      nil  -> {:error, "Post id #{id} not found"}
      post -> {:ok, post}
    end
  end

  def create(args, _info) do
    %Post{}
    |> Post.changeset(args)
    |> Repo.insert
  end

  def update(%{id: id, post: post_params}, _info) do
    post      = Repo.get!(Post, id)
    changeset = Post.changeset(post, post_params)

    case Repo.update(changeset) do
      {:ok, post} -> {:ok, post}
      {:error, _changeset} -> {:error, message: "Post update failed", id: id}
    end
  end

  def delete(%{id: id}, _info) do
    case Repo.get(Post, id) do
      nil  -> {:error, message: "Post delete failed", id: id}
      post -> Repo.delete(post)
    end
  end
end
